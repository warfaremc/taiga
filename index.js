// Resource: SupportBot
// Developed by Emerald
// Version: 3.2 - Customized

const Discord = require("discord.js");
const fs = require("fs");
const bot = new Discord.Client();

bot.commands = new Discord.Collection();
bot.settings = require("./settings.json");

bot.on('error', err => console.log(err));

fs.readdir("./commands/", (err, files) => {
    if(err) console.log(err);

    let jsfile = files.filter(f => f.split(".").pop() === "js");
    if(jsfile.length <= 0) return console.log(`${bot.settings.botname} No commands found. Try to Re-download the resource`);

    jsfile.forEach((f, i) =>{
        let props = require(`./commands/${f}`);
        console.log(`[${bot.settings.botname}] > Command Loaded > ${f}`);
        bot.commands.set(props.help.name, props);
    });

});

bot.addons = new Discord.Collection();

fs.readdir("./sb-addons/", (err, files) => {
    if(err) console.log(err);

    let jsfile = files.filter(f => f.split(".").pop() === "js");
    if(jsfile.length <= 0) return console.log(`${bot.settings.botname} No addons detected nor default! Either download addons from https://itzemerald.tk/`);

    jsfile.forEach((f, i) =>{
        let props = require(`./sb-addons/${f}`);
        console.log(`[${bot.settings.botname}] > Addons Loaded > ${f}`);
        bot.addons.set(props.help.name, props);
    });

});

bot.on("ready", async () => {
    bot.user.setActivity(bot.settings.BotStatus, {type:"PLAYING"});
    console.log(`\x1b[36m`, `=== [SupportBot] ===\n\nSupportBot - Created by Emerald Services and edited by Mirai\nInvite ${bot.user.username} to your server\nhttps://discordapp.com/api/oauth2/authorize?client_id=${bot.user.id}&permissions=8&scope=bot\n\n=== [SupportBot] ===`)
});

bot.on("message", async message => {
    if(message.author.bot) return;
    if(message.channel.type === "dm") return;
    if (message.content.indexOf(bot.settings.prefix) !== 0) return;

    let messageArray = message.content.split(" ");
    const args = message.content.slice(bot.settings.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    const cmd = bot.commands.get(command);
    if(!cmd) return;
    cmd.run(bot, message, args);
});

bot.on("message", async message => {
    if(message.author.bot) return;
    if(message.channel.type === "dm") return;
    if (message.content.indexOf(bot.settings.prefix) !== 0) return;

    let messageArray = message.content.split(" ");
    const args = message.content.slice(bot.settings.prefix.length).trim().split(/ +/g);
    const addon = args.shift().toLowerCase();

    const cmd = bot.addons.get(addon);
    if(!cmd) return;
    cmd.run(bot, message, args);
});

bot.on('voiceStateUpdate', (oldMember, newMember) => {
    let newUserChannel = newMember.voiceChannel;
    let oldUserChannel = oldMember.voiceChannel;

    if (newUserChannel !== undefined && newUserChannel.id === "610235436559761461") {

        var guild = newMember.guild;

        guild.createChannel(newMember.user.username + " Voice", 'voice').then(
            chan => {
                chan.setParent("568776047860908055");
                newMember.setVoiceChannel(chan);
            }
        )
    }
    if (oldUserChannel === undefined) {

    } else {
        if (oldUserChannel.id === "610235436559761461") { return; }
    }
    if(newUserChannel === undefined){
        if(oldUserChannel !== undefined){
            if (oldUserChannel.parent !== undefined) {
                if (oldUserChannel.parent.id === "568776047860908055") {
                    if(oldUserChannel.members.size === 0){
                        oldUserChannel.delete("Voice leave");
                    }
                }
            }
        }

    }
    if(newUserChannel !== undefined) {
        if (oldUserChannel !== undefined) {
            if (oldUserChannel.parent !== undefined) {
                if (oldUserChannel.parent.id === "568776047860908055") {
                    if (oldUserChannel.members.size === 0) {
                        oldUserChannel.delete("Voice leave");
                    }
                }
            }
        }

    }});

bot.login(bot.settings.token);