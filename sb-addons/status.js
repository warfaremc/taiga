// For use with Node.js
const Discord = require("discord.js");
const bot = new Discord.Client()

bot.settings = require("../settings.json");
exports.run = async(bot, message, args) => {
    message.delete();
	var ms = require('../minestat');
ms.init('82.208.17.17', 27841, function(result)
{
  if(ms.online)
  {
	    const statusEmbed = new Discord.RichEmbed()
        .setTitle(":eyes: Stav serveru")
        .addField(":man: Hráči", ms.current_players + "/" +  ms.max_players, true)
        .addField(":rocket: Stav", "Online", true)
        .addField(":one: Verze", ms.version + "", true)
		.addField(":ping_pong: Ping", ms.latency + " ms", true)
        .setColor("#00ff04")
        .setFooter(bot.settings.footer)
		message.channel.send({embed: statusEmbed});
  }
  else
  {
    	    const statusEmbed = new Discord.RichEmbed()
        .setTitle(":eyes: Stav serveru")
        .addField(":man: Hráči", "0/0", true)
        .addField(":rocket: Stav", "Offline :(", true)
        .addField(":one: Verze", "N/A", true)
		.addField(":ping_pong: Ping", "0 ms", true)
        .setColor("#ff0000")
        .setFooter(bot.settings.footer)
		message.channel.send({embed: statusEmbed});
  }
});
}
exports.help = {
    name: 'status',
}
