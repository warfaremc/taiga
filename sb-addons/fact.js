// For use with Node.js
const Discord = require("discord.js");
const bot = new Discord.Client()

bot.settings = require("../settings.json");
exports.run = async(bot, message, args) => {
    message.delete();
	var textArray = [
    'Podle Bible poslal Bůh 2 medvědy, aby zavraždili 42 dětí, protože se posmívali plešatému.',
    'Když je člověk zamilovaný, mozek ignoruje nedostatky milovaného člověka a je pro ně obtížné opustit ho, i když mu způsobuje bolest.',
	'V 60. letech podávali vědci delfínům LSD. Chtěli je naučit anglicky.',
	'Sprchovat se nebo mýt nádobí při bouřce není vhodné. Míle daleko může uhodit blesk a přenese se přes vodovodní trubky.',
	'Humrové poslouchají nohama.',
	'Čínský znak pro potíže je zobrazován, jako dvě ženy pod jednou střechou.',
	'V řadě supermarketů najdete oddělení pečiva nejblíže ke dveřím, vůně čerstvého chleba povzbuzuje lidi k nákupu.',
	'Plechovka dietní Coca-Coly plave, zatímco klasická Coca-Cola klesá ke dnu.',
	'Myšlením denně spalíte zhruba 50 kalorií.',
	'Během natáčení filmu Borat volali lidé 91krát policii.',
	'V Rusku žila žena, která porodila celkem 69 dětí. Těhotná byla 27x a měla 16x dvojčata, 7x trojčata a 4x čtyřčata.',
	'Slon může zemřít na zlomené srdce.',
	'Turritopsis nutricula je druh medůz, který je nesmrtelný.',
	'Neexistují dva stejné kukuřičné lupínky.',
	'Lidské tělo vytvoří za 30 minut dostatek tepla na to, aby přivedlo 2 litry vody k bodu varu.',
	'V Singapuru je nezákonné prodávat žvýkačky.',
	'Pokud se dítě narodí na palubě letadla, může získat občanství země, ve které je zapsáno sídlo letecké společnosti.',
	'Pro muže je mnohem težší být ""jen přátelé"" s opačným pohlavím než pro ženy.',
    'V New Yorku byste mohli chodit do konce života na oběd a pokaždé jíst v jiné restauraci.',
    'Na světe je více slepic než lidí.',
    'Být svobodný může muži zkrátit život o 10 let.',
    'V Turkmenistánu je od roku 1991 zdarma voda, plyn a elektřina.',
    'Pokud by byl mravenec stejně velký jako člověk, pak by se pohyboval rychlostí zhruba 600km/hod.',
    '84 % žen má rádo muže v růžovém tričku. Paradoxně 87 % mužů v růžovém tričku nemá rádo ženy.',
    'Víte proč je jablíčko společnosti Apple nakousnuté? To proto, aby se logo nemohlo zaměnit s třešní.',
    'Zub je jediná část lidského těla, která se sama nedovede zahojit.',
    'Ženy v průměru pláčou 30x až 64x za rok. Muži pláčou max. 6x ročně.',
    'Lidé, kteří často masturbují bývají méně deprimovaní.',
    'Během vašich narozenin zemře zhruba 153.000 lidí.',
    'Některé hlavní značky řasenek obsahují jako přísadu netopýří trus.',
    'Synovec Adolfa Hitlera utekl z Německa do Ameriky, narukoval do amerického námořnictva a bojoval proti Německu.',
    'Celosvětově se nejvíce v obchodech s potravinami kradou sýry.',
    'Řidiči ročně zabíjí více jelenů než lovci.',
    'V roce 1970 bral ředitel firmy 39 krát větší plat, než průměrný pracovník. Dnes bere 1039 krát více.',
    'Naše rty jsou 100x citlivější, než konečky prstů na ruce.',
    'Mořské vydry se ve spánku na vodě drží za packy, aby si neuplavaly.',
    'Během doby co čtete tento fakt, ve vašem těle zemře 50.000 buněk a je nahrazeno novými.',
    'Krokodýl nemůže vystrčit jazyk ani s ním nijak volně pohybovat, protože ho má přirostlý ke spodnímu patru.',
    'Jen 2 % žen se považuje za krásné.',
    'Jeden japonský voják zůstal skrytý 29 let po konci 2. Světové války, protože nevěděl, že už skončila.',
    'Slovo Whatever, v angličtině, je nejotravnější slovo. Vyplívá to z U.S. poll, která byla pořádána v naposledy roce 2016 po osmé za sebou.',
    'Hurikán Katrina snížil počet jmén Katrina o 85%',
    'Jednou z hlavních atrakcí v Zoo Pyongyang, v Jížní Korei, je šimpanz, který vykouří jeden balík cigaret za jeden den',
    'Jeden ze sloganů Richarda Nixona v roce 1972 byl You Cant Lick our Dick.',
    'Španělská fráze ¿Cómo como? ¡Como como como! znamená Jak mám jíst? Já jím tak, jak já jím!',
    'Rekord lidí převlečených za Einsteina na jednom místě je 404.',
    'Sri Lanka má Ministerstvo vývoje kokosů.',
    'V Oregonu and New Jersey, je legální dávat do auta vlastní benzín.',
    '47 Sovietů zemřelo pro 2 americké vojáky za druhé světové války.'
];
var randomNumber = Math.floor(Math.random()*textArray.length);
	  const embed = new Discord.RichEmbed()
        .setTitle('🏆 **Fakt**')
        .setDescription(textArray[randomNumber])
        .setTimestamp()
        .setColor(bot.settings.colour)
        .setFooter(bot.settings.footer)
	message.channel.send(embed);
}
exports.help = {
    name: 'fact',
}
