// SupportBot
// Command: Close Ticket

const Discord = require("discord.js");
const bot = new Discord.Client()

bot.settings = require("../settings.json");

exports.run = (bot, message, args) => {
    message.delete();

    const outsideticket = new Discord.RichEmbed()
    .setDescription(`:x: Tento příkaz nelze použít v jiném kanálu.`)
    .setColor(bot.settings.colour) 
if (!message.channel.name.startsWith(`ticket-`)) return message.channel.send({embed: outsideticket});
const close1 = new Discord.RichEmbed()
    .setDescription(`Vypadá to, že jsme se dostali na konec ticketu\nPotvrď, že chceš ticket uzavřít napsáním **uzavřít**`)
    .setFooter("Tvoje žádost bude zrušena za 20 sekund")
    .setColor(bot.settings.colour)
message.channel.send({embed: close1}).then(m => {
    message.channel.awaitMessages(response => response.content === `uzavřít`, {
        max: 1,
        time: 10000,
        errors: ['time'],

    }).then((collected) => {
		
				let logChannel = message.guild.channels.find(TicketChannel => TicketChannel.name === `${bot.settings.Ticket_Logs}`);
    if(!logChannel) return message.channel.send(`:x: Error! Could not find the logs channel **${bot.settings.Ticket_Logs}**`);
	    const logEmbed = new Discord.RichEmbed()
        .setTitle(":ticket: Informace o ticketu")
        .setDescription("✅ Ticket **" + message.channel.name + "** byl uzavřen.")
        .setColor("#00c10c")
        .setFooter(bot.settings.footer)
		logChannel.send({embed: logEmbed})
		
        message.channel.delete();
    
   

    }).catch(() => {
        m.edit('Nebylo odpovězeno, příkaz zrušen.').then(m2 => {
        m2.delete();
    }, 3000);

    });
});

console.log(`\x1b[36m`, `${message.author} has executed ${bot.settings.prefix}${bot.settings.Close_Command}`)

}

exports.help = {
    name: bot.settings.Close_Command,
}