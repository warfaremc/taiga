// SupportBot
// Command: Help

const Discord = require("discord.js");
const bot = new Discord.Client()

bot.settings = require("../settings.json");

exports.run = (bot, message, args) => {
    message.delete();

    let userCommands = "";
        userCommands += `**${bot.settings.prefix}${bot.settings.Link_Command}**: Nějaké ty odkazy!\n`;
        userCommands += `**${bot.settings.prefix}${bot.settings.Ping_Command}**: Zjistí odezvu bota.\n`;
	    userCommands += `**${bot.settings.prefix}${bot.settings.Ticket_Command} <důvod>**: Vytvoří ticket\n`;
        userCommands += `**${bot.settings.prefix}${bot.settings.Close_Command}**: Uzavře tvůj ticket\n`;
        userCommands += `**${bot.settings.prefix}${bot.settings.Suggest_Command} <návrh>**: Vytvoří návrh na zlepšení serveru\n`;
		userCommands += `**${bot.settings.prefix}status**: Zobrazí stav serveru\n`;
    userCommands += `**${bot.settings.prefix}fact**: Pošle náhodný fakt\n`;
	
    let staffCommands = "";
	    staffCommands += `**${bot.settings.prefix}${bot.settings.Add_Command} <uživatel>**: Přidá uživatele do ticketu\n`;
        staffCommands += `**${bot.settings.prefix}${bot.settings.Remove_Command} <uživatel>**: Odebere uživatele z ticketu\n`;
        staffCommands += `**${bot.settings.prefix}${bot.settings.Forceclose_Command}**: Uzavře ticket bez jednání, okamžitě.\n`;
		staffCommands += `**${bot.settings.prefix}${bot.settings.Rename_Command}**: Přejmenuje ticket\n`;
        staffCommands += `**${bot.settings.prefix}${bot.settings.Say_Command} <message>**: Vytvoří oznámení v kanálu\n`;
        staffCommands += `**${bot.settings.prefix}${bot.settings.Lockchat_Command}**: Uzavře kanál\n`;
        staffCommands += `**${bot.settings.prefix}${bot.settings.UnLockchat_Command}**: Odemkne kanál\n`;

    const embed = new Discord.RichEmbed()
        .setTitle(bot.settings.botname)
        .addField(":ticket: Základní příkazy", userCommands)
        .addField(":pushpin: Příkazy administártorů", staffCommands)
        .setColor(bot.settings.colour)
        .setFooter(bot.settings.footer, message.author.displayAvatarURL);

	message.channel.send(embed)

    console.log(`\x1b[36m`, `${message.author} has executed ${bot.settings.prefix}${bot.settings.Help_Command}`)

}

exports.help = {
    name: bot.settings.Help_Command,
}